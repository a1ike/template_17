$(document).ready(function () {

  $('form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var avtor = $('input[name="avtor"]', f).val();
    var tel = $('input[name="tel"]', f).val();
    var email = $('input[name="e-mail"]', f).val();
    var error = false;
    if (tel == '') {
      $('input[name="avtor"]', f).addClass('ierror');
      error = true;
    }
    if (tel == '') {
      $('input[name="tel"]', f).addClass('ierror');
      error = true;
    }
    if (email == '') {
      $('input[name="e-mail"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        window.location.href = 'https://docs.google.com/spreadsheets/d/1s28svuFn5OaqjAQ597vgi4mEz8GgdcKhDna_u1Vpt9Q/edit#gid=661409618';
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

});
